'use strict';

import angular from 'angular';
import {
  UtilService
} from './util.service';

export default angular.module('adwangularApp.util', [])
  .factory('Util', UtilService)
  .name;
